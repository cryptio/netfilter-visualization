# Copyright 2020 cpke

from RPi import GPIO
import time
import sys


def main():
    required_arg_num = 2
    if len(sys.argv) < required_arg_num:
        print('Expected {} arguments, received {}'.format(required_arg_num, len(sys.argv)))
        sys.exit(1)

    action = sys.argv[1]

    pin_action_mappings = {
        'PKT_ACCEPTED': 27,
        'PKT_DENIED': 17
    }

    if action not in pin_action_mappings:
        print('{} is not a valid action, must be one of: {}'.format(action, list(pin_action_mappings.keys())))
        sys.exit(1)

    output_pin = pin_action_mappings[action]

    GPIO.setmode(GPIO.BCM)  # refer to pins by Broadcom SOC channel, not physical pin number
    GPIO.setwarnings(False)

    GPIO.setup(output_pin, GPIO.OUT)
    GPIO.output(output_pin, True)

    time.sleep(0.01)  # 10 milliseconds

    GPIO.cleanup()


if __name__ == '__main__':
    main()
