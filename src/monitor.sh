# Loopback interface
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# We create new chains for acceptance and denial. This way, packets can be logged before an action is taken on them (accept/drop)
iptables -N LOG_ACCEPT
iptables -N LOG_DENY

# ICMP
iptables -A INPUT -p icmp -j LOG_DENY

# SSH
iptables -A INPUT -p tcp --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -j LOG_ACCEPT
iptables -A OUTPUT -p tcp --sport 22 -m conntrack --ctstate ESTABLISHED -j LOG_ACCEPT

# DNS
iptables -A OUTPUT -p udp --dport 53 -m conntrack --ctstate NEW,ESTABLISHED -j LOG_ACCEPT
iptables -A INPUT -p udp --sport 53 -m conntrack --ctstate ESTABLISHED -j LOG_ACCEPT

# Log each packet that gets accepted, then accept it
iptables -A LOG_ACCEPT -j LOG --log-prefix="PKT_ACCEPTED"
iptables -A LOG_ACCEPT -j ACCEPT

# Log each packet that gets denied, then drop it
iptables -A LOG_DENY -j LOG --log-prefix="PKT_DENIED"
iptables -A LOG_DENY -j DROP

# The "LOG" target will output to kern.log. We monitor this log for new packets being accepted/denied, and then respectively run a Python script that will turn on the LED
tail -f /var/log/kern.log | awk 'match($0, /PKT_ACCEPTED|PKT_DENIED/) {system("python3 /home/pi/netfilter-visualize.py " substr($0, RSTART, RLENGTH))}' & 