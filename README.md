# netfilter Visualization

This project uses a Raspberry Pi to visualise netfilter's activity by triggering LEDs when a packet is either accepted or rejected.

## Setup
1. Place the `netfilter-visualize.py` script on your system
2. Attach 2 LEDs to your Raspberry Pi's GPIO pins: one to represent packet acceptance, and the other to represent packet denial. If necessary, modify the `netfilter-visualize.py` script to use the GPIO pin numbers you chose. By default, pin 27 is for accepted packets, and pin 17 is for rejected packets. Note that these numbers refer to the Broadcom SOC channel, and not the physical pin numbers themselves.
3. Give the `monitor.sh` executable permissions and run it. This will setup a basic firewall that blocks ICMP and only allows inbound SSH on the default port as well as outbound DNS traffic. It is merely an example, and more rules can be added as long as they are using either the `LOG_ACCEPT` or `LOG_DENY` action. If your `netfilter-visualize.py` script is not located at `/home/pi/netfilter-visualize.py`, make sure to change that in `monitor.sh` to reflect the proper file location.

## Usage
While this does not have much practical usage, it is useful to have a visual representation of how packets are being dealt with on the system. One may use this to develop rules to block certain types of traffic, such as specific DDoS attacks, and then simulate the traffic in realtime and see whether or not the expected result is happening using the LEDs.

## Caveats
tail may not be able to keep up with the rate of log entries coming in each second, which can lead to delays in the LEDs blinking. In this case, you may wish to keep track of the last time that a `LOG_ACCEPTED` or `LOG_DENIED` entry came in, and just keep the LED on if the difference is less than 1 second.